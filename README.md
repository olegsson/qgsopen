# qgsopen

Load local files to a running QGIS instance from external Python code. Because why not.

## Install

### Requirements

  - Python >= 3.6
  
  - a working [QGIS][1] installation
  
  - OS support for [UNIX sockets][2]

### Install the client

`python -m pip install git+https://gitlab.com/olegsson/qgsopen.git`

(no PyPI for now)

### Connect with QGIS

#### Option 1

If you've installed the package into a Python environment shared with QGIS you can add the following line to your [startup file][3] to start the server:

`import qgsopen.server`

#### Option 2

Clone the repository (`git clone https://gitlab.com/olegsson/qgsopen.git`) and run your QGIS instance with:

`qgis --code <path-to-repository>/qgsopen/server.py`

Or alternatively download [`server.py`][4] (e.g. with `wget https://gitlab.com/olegsson/qgsopen/-/raw/master/qgsopen/server.py`) and point the `--code` option to it.

## Usage

After ensuring your QGIS instance runs with `qgsopen.server` (see above), you can use the client to open an arbitrary number of GDAL/OGR readable local files at once with:

```
from qgsopen import qgso

qgso(
	'/path/to/file1.tif',
	'/path/to/file2.gpkg',
	...,
	style=[ # optional
		'/path/to/style1.qml',
		'/path/to/style2.qml',
		...
	]
)
```

`style` can also be passed as a single QML path. If the number of `style`s is lesser than the number of layers, `qgso` will attempt to apply the last QML on the list to all layers not explicitly provided with one (e.g. if you provide a single QML path it will be applied to all provided layers).

## License

[BEER-WARE][5]

(As the implementation of this software is fairly trivial and it is technically not a plugin, the author did not feel the need to implement the more elaborate GPL >= 2 licensing. That said, if any of the fine people from the QGIS community feel that this violates the [QGIS plugin licensing requirements][6] they are free to correct me.)

[1]: https://www.qgis.org
[2]: https://en.wikipedia.org/wiki/Unix_domain_socket
[3]: https://docs.qgis.org/3.22/en/docs/pyqgis_developer_cookbook/intro.html#running-python-code-when-qgis-starts
[4]: ./qgsopen/server.py
[5]: ./LICENSE
[6]: https://blog.qgis.org/2016/05/29/licensing-requirements-for-qgis-plugins/
