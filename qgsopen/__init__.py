__version__ = '0.3.1'

from pathlib import Path
from typing import Union, Iterable
import json, socket, tempfile, warnings

SOCK_PATH = Path(tempfile.gettempdir()) / 'qgsopen.sock'

def normalize_path(path: Union[Path, str]):
    return str(Path(path).absolute())

def qgso(
    *file_paths: Iterable[Union[Path, str]],
    style: Iterable[Union[Path, str]]=None,
):
    import requests

    if isinstance(style, (str, Path)):
        style = [style]
    
    request = json.dumps({
        'paths': [*map(normalize_path, file_paths)],
        **({
            'styles': [*map(normalize_path, style)]
        } if style is not None else {}),
    }) + '\n'

    with socket.socket(
        socket.AF_UNIX,
        socket.SOCK_STREAM,
    ) as sock:
        try:
            sock.connect(str(SOCK_PATH))
            sock.sendall(request.encode())
            response = json.loads(sock.recv(1024).decode())

            if response['errors'] != []:
                warnings.warn(
                    'qgsopen server responded with errors:' \
                    ' / '.join(response['errors'])
                )

        except ConnectionRefusedError:
            warnings.warn('QGIS instance not found')

