from threading import Thread
from pathlib import Path
from typing import Union, Iterable
import json, asyncio, tempfile

SOCK_PATH = Path(tempfile.gettempdir()) / 'qgsopen.sock'

class SourceError(Exception):
    '''
    Raise on invalid layer source.
    '''
    def __init__(self, msg):
        pass

class StyleError(Exception):
    '''
    Raise on invalid style source.
    '''
    def __init__(self, msg):
        pass

def load_layer(
    layer_path: Union[str, Path],
    style_path: Union[str, Path]=None,
):
    '''
    Load layer into the active QGIS project,
    optionally with a QML style.
    '''
    from qgis.core import QgsProject, QgsRasterLayer, QgsVectorLayer

    for layer_class in (
        QgsRasterLayer,
        QgsVectorLayer,
    ):
        try:
            layer = layer_class(
                layer_path,
                Path(layer_path).stem,
            )
            assert layer.isValid()

            style_noerror = True
            if style_path is not None:
                style_error_msg, style_noerror = layer.loadNamedStyle(style_path)

            QgsProject.instance().addMapLayer(layer)

            if not style_noerror:
                raise StyleError(
                    f'{style_error_msg}: {style_path}',
                )
            return
        
        except AssertionError:
            continue

    raise SourceError(
        f'invalid or unsupported layer source: {layer_path}',
    )

def load_all_layers(
    layer_paths: Iterable[Union[str, Path]],
    style_paths: Iterable[Union[str, Path]],
) -> Iterable[Exception]:
    errors = []
    for layer_path, style_path in zip(
        layer_paths,
        style_paths
    ):
        try:
            load_layer(layer_path, style_path)
        except Exception as e:
            errors.append(e)
    return errors
    
async def request_handler(
    inp: asyncio.StreamReader,
    out: asyncio.StreamWriter,
):
    while True:
        response = {
            'errors': [],
        }
        
        request = json.loads((await inp.readline()).decode())

        try:
            layer_paths = request['paths']
        except KeyError:
            response['errors'].append('bad request')
            out.write(json.dumps(response).encode())
            await out.drain()
            continue

        try:
            style_paths = request['styles']
        except KeyError:
            style_paths = [None]

        # if N styles < N layers populate the list
        # with the last style submitted
        style_paths += style_paths[-1:] * (
            len(layer_paths) - \
            len(style_paths)
        )

        loop = asyncio.get_running_loop()
        errors = await loop.run_in_executor(
            None,
            load_all_layers,
            layer_paths,
            style_paths,
        )
        
        response['errors'] = [*map(str, errors)]
        out.write(json.dumps(response).encode())
        await out.drain()

async def run_server(sock_path):
    async with await asyncio.start_unix_server(
        request_handler,
        sock_path,
    ) as server:
        await server.serve_forever()
        
Thread(
    target=asyncio.run,
    args=(run_server(SOCK_PATH),),
).start()
