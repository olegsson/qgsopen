import setuptools
from pathlib import Path

from qgsopen import __version__

with open(
    Path(__file__).parent/'README.md'
) as src:
    long_description = src.read()

setuptools.setup(
    name='qgsopen',
    version=__version__,
    author='Luka Antonić',
    keywords='geospatial, QGIS',
    description='Open files in a running QGIS instance from Python code',
    long_description=long_description,
    url='https://gitlab.com/olegsson/qgsopen',
    packages=setuptools.find_packages(),
    python_requires='>=3.6, <4',
    install_requires=[],
)
